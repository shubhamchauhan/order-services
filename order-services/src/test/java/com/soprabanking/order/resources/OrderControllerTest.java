package com.soprabanking.order.resources;

import static org.mockito.Mockito.times;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.soprabanking.order.model.Order;
import com.soprabanking.order.repository.OrderRepo;
import com.soprabanking.order.service.OrderServiceImpl;
import com.soprabanking.order.service.SequenceGeneratorService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@WebFluxTest(controllers = OrderController.class)
@Import(OrderServiceImpl.class)
class OrderControllerTest {

	@Autowired
	private WebTestClient webClient;

	@MockBean
	private SequenceGeneratorService sequenceGeneratorService;

	@MockBean
	private OrderRepo orderRepo;

	@Test
	void testFindAll() {
		
		Mockito.when(orderRepo.findAll()).thenReturn(Flux.just(new Order(100, "Test Crocin 650", 10, 20, "Shubham", "10, Shanti Niketan, Meerut", "PENDING"),
				new Order(101, "Test D-Cold 500", 5, 15, "Anant", "65, Vidya Niketan, Noida", "PENDING")));

		webClient.get().uri("/pharmacy/order/getAllOrders")
				.exchange()
				.expectStatus()
				.isOk()
				.expectBodyList(Order.class);

		Mockito.verify(orderRepo, times(1)).findAll();
	}
	 

	@Test
	void testGetOrderById() {
		Order order = new Order();
		order.setId(100);
		order.setMedicineName("Test Crocin 650");
		order.setOrderStatus("PENDING");
		order.setQuantity(10);
		order.setToAddress("10, Shanti Niketan, Meerut");
		order.setTotalAmount(20);
		order.setUserName("Shubham");

		Mockito.when(orderRepo.findById(100)).thenReturn(Mono.just(order));

		webClient.get().uri("/pharmacy/order/getOrder/{id}", 100)
				.exchange()
				.expectStatus()
				.isOk()
				.expectBody()
				.jsonPath("$.medicineName").isNotEmpty()
				.jsonPath("$.id").isEqualTo(100)
				.jsonPath("$.medicineName")
				.isEqualTo("Test Crocin 650")
				.jsonPath("$.totalAmount").isEqualTo(20);

		Mockito.verify(orderRepo, times(1)).findById(100);
	}
	
	@Test
	void testGetOrdersByUserName() {
		
        Mockito
            .when(orderRepo.findOrdersByUserName("Anant"))
            .thenReturn(Flux.just(new Order(150, "Test D-Cold 650", 10, 20, "Anant", "10, Shanti Niketan, Meerut", "PENDING")));
 
        webClient.get().uri("/pharmacy/order/getOrdersByUserName/{userName}", "Anant")
            .header(HttpHeaders.ACCEPT, "application/json")
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(Order.class);
         
        Mockito.verify(orderRepo, times(1)).findOrdersByUserName("Anant");
	}

	@Test
	void testDeleteOrder() {
		Mono<Void> voidReturn  = Mono.empty();
		
		Mockito
			.when(orderRepo.deleteById(100))
			.thenReturn(voidReturn);
		
		webClient.delete()
			.uri("/pharmacy/order/deleteOrder/{id}", 100)
			.exchange()
			.expectStatus()
			.isOk();
	}

	// @Test
		// void testSaveNewOrder() throws Exception {

		/*
		 * Medicine medicine = new Medicine(); medicine.setId(50);
		 * medicine.setMedicineName("Test Crocin 250"); medicine.setCompanyName("GSK1");
		 * //medicine.setExpiryDate(new Date("2020-06-12")); medicine.setQuantity(100);
		 * medicine.setAmount(2);
		 * 
		 * Order order = new Order(); order.setId(100);
		 * order.setMedicineName(medicine.getMedicineName());
		 * order.setOrderStatus(GlobalConstants.DEFAULT_ORDER_STATUS);
		 * order.setQuantity(10); order.setToAddress("10, Shanti Niketan, Meerut");
		 * order.setTotalAmount(20); order.setUserName("Shubham");
		 * 
		 * MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(
		 * "/pharmacy/order/addNewOrder/{id}", medicine.getId())
		 * .content(order.toString()) .contentType(MediaType.APPLICATION_JSON)
		 * .accept(MediaType.APPLICATION_JSON)) .andExpect(status().isOk())
		 * .andReturn();
		 * 
		 * String resultSS = result.getResponse().getContentAsString();
		 * assertNotNull(resultSS); assertEquals(order.toString(), resultSS);
		 */
		/*
		 * when(orderService.save(50, order)).thenReturn(Mono.just(order));
		 * 
		 * webClient.post() .uri("/pharmacy/order/addNewOrder/{id}", 50)
		 * .contentType(MediaType.APPLICATION_JSON) .bodyValue(order)
		 * //(BodyInserters.fromValue(order)) .exchange() .expectBodyList(Order.class);
		 */

		// }
}
