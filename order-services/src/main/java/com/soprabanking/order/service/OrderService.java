package com.soprabanking.order.service;

import org.springframework.http.ResponseEntity;

import com.soprabanking.order.model.Order;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface OrderService {

	Mono<Order> save(int id, Order order);

	Flux<Order> findAll();

	Mono<Order> getOrder(int id);

	Flux<Order> getOrdersByUserName(String userName);
	
	Mono<Order> updateOrder(int id, Order order);

	Mono<ResponseEntity<Void>> deleteOrder(int id);
}
