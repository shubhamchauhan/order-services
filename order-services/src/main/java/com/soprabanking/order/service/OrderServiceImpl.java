package com.soprabanking.order.service;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.soprabanking.order.exception.ResourceNotFoundException;
import com.soprabanking.order.model.Medicine;
import com.soprabanking.order.model.MedicineInfoEvent;
import com.soprabanking.order.model.Order;
import com.soprabanking.order.repository.OrderRepo;
import com.soprabanking.order.util.GlobalConstants;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class OrderServiceImpl implements OrderService {

	/*
	 * @Autowired private RestTemplate restTemplate;
	 */
	// Adding default slf4j logging
	Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);

	@Autowired
	private OrderRepo orderRepo;

	@Autowired
	private SequenceGeneratorService sequenceGeneratorService;

	private Medicine medicine;

	@Autowired
	private WebClient.Builder webClientBuilder;

	/**
	 * @param id
	 * @param order
	 * @return
	 * @throws ResourceNotFoundException
	 */
	@Override
	public Mono<Order> save(int id, Order order) {

		order.setId(sequenceGeneratorService.generateSequence(GlobalConstants.ORDERS_SEQUENCE));
		medicine = webClientBuilder.build().get().uri(GlobalConstants.URL_GET_MEDICINE_BY_ID + id).retrieve()
				.bodyToMono(Medicine.class).block();

		/*
		 * medicine = restTemplate.getForObject(GlobalConstants.URL_GET_MEDICINE_BY_ID +
		 * id, Medicine.class);
		 */

		if (order.getQuantity() > medicine.getQuantity()) {
			System.out.println("Quantity exceeds availablity, order failed");
		}
		medicine.setQuantity(medicine.getQuantity() - order.getQuantity());
		/*
		 * webClientBuilder.build().put().uri(GlobalConstants.URL_UPDATE_MEDICINE + id,
		 * medicine).retrieve() .bodyToMono(Medicine.class).block();
		 */
		webClientBuilder.build().put().uri(GlobalConstants.URL_UPDATE_MEDICINE + id).body(Mono.just(medicine), Medicine.class)
		.retrieve().bodyToMono(Medicine.class).block();
		order.setTotalAmount(medicine.getAmount() * order.getQuantity());

		return orderRepo.save(order);
	}

	@Override
	public Flux<Order> findAll() {
		log.info("Order list is asked to be retrieved");
		return orderRepo.findAll();
	}

	@Override
	public Mono<Order> getOrder(int id) {
		log.info("Retrieve order with id: " + id);
		return Mono.just(id)
				.flatMap(orderRepo::findById)
				.switchIfEmpty(Mono.error(new ResourceNotFoundException("No order found for this id :: " + id)));
	}

	@Override
	public Flux<Order> getOrdersByUserName(String userName) {
		log.info("Retrieve all orders for the user: " + userName);
		return orderRepo.findOrdersByUserName(userName);
	}
	
	@Override
	public Mono<Order> updateOrder(int id, Order order) {
		log.info("Update order with id: " + id);
		Mono<Order> existingOrder = orderRepo.findById(id);
		if (Objects.isNull(existingOrder))
			return Mono.error(new ResourceNotFoundException("No order found for this id :: " + id));
		return orderRepo.save(order);
	}

	@Override
	public Mono<ResponseEntity<Void>> deleteOrder(int id) {
		log.info("Delete Order with id: " + id);
		return orderRepo.deleteById(id).then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK)));
	}

	@KafkaListener(topics = "Kafka_Example", groupId = "group_id", containerFactory = "kafkaListenerContainerFactory")
	public void consumeJson(MedicineInfoEvent medicineInfo) {
		System.out.println("Consumed JSON Message: " + medicineInfo);
	}
}
